### Enable touchpad
xinput --set-prop 'SynPS/2 Synaptics TouchPad' 'Device Enabled' 1

### xrandr extend s (--above)
bash -c "$(read edp hdmi <<<$(xrandr --listmonitors | tail -n +2 | awk '{print $NF}') && xrandr --output "$hdmi" --above "$edp")"

### xrandr duplicate (--same-as)
bash -c "$(read edp hdmi <<<$(xrandr --listmonitors | tail -n +2 | awk '{print $NF}') && xrandr --output "$hdmi" --same-as "$edp")"

### Ansible system maintenance
bash -c "$(grep -P --color -o '(?<=system-maintenance.daily).*' /etc/anacrontab | sh -)"

### screen OFF (brightness 0)
sudo sh -c 'echo 0 > /sys/class/backlight/intel_backlight/brightness'

### screen ON (brightness)
sudo sh -c 'echo 150 > /sys/class/backlight/intel_backlight/brightness'
